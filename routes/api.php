<?php

use Illuminate\Http\Request;
use App\Group;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('jwt.auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');
Route::get('reports', 'ReportController@stocksWeeklyReport');
Route::get('del', 'ReportController@cleanStorage');
Route::get('mos', 'ReportController@mos');


$api = app('Dingo\Api\Routing\Router');
//Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
//
// -$api->version('v1', ['middleware' => 'jwt.auth','namespace' => 'App\Http\Controllers'], function ($api) {

// $api->version('v1', function($api)
$api->version('v1', ['middleware' => 'jwt.auth','namespace' => 'App\Http\Controllers'], function ($api) {
    // middleware variable for roles
    $admin_middleware = 'role:'.Group::ADMIN;
    $epi_admin_middleware = 'role:'.Group::EPI_ADMIN;
    $epi_middleware = 'role:'.Group::EPI;

    $api->group(['middleware' => $admin_middleware], function($api){
        $api->resource('counties', 'CountyController');
        $api->resource('users', 'UserController');
    });

    $api->resource('facilities', 'FacilityController');
    $api->resource('subcounties', 'SubcountyController');
    $api->resource('regions', 'RegionController');
    $api->resource('stores', 'StoreController');
    $api->resource('storesfridges', 'StoreFridgeController');
    $api->resource('fridges', 'FridgeController');
    $api->resource('locations', 'LocationController');

});

Route::get('stock_doses', 'StockController@getStockDoses');
