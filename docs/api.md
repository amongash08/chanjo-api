# API Spec

## Authentication Header:
Authorization: Bearer token.here

## JSON Objects returned by API:
### Multiple Regions
```JSON
{
    "data": [
        {
            "id": 1,
            "region_name": "Pattie Heaney"
        },
        {
            "id": 2,
            "region_name": "Melba Crona"
        },
        {
            "id": 3,
            "region_name": "Clint Senger"
        },
        {
            "id": 4,
            "region_name": "Ms. Maritza Paucek PhD"
        },
        {
            "id": 5,
            "region_name": "Hazle Morissette II"
        },
        {
            "id": 6,
            "region_name": "Dessie Stark III"
        },
        {
            "id": 7,
            "region_name": "Jewel Lemke"
        },
        {
            "id": 8,
            "region_name": "Sheldon Bergnaum"
        },
        {
            "id": 9,
            "region_name": "Nya Oberbrunner DVM"
        },
        {
            "id": 10,
            "region_name": "Earnestine Gottlieb"
        },
        {
            "id": 11,
            "region_name": "Sage Bailey"
        },
        {
            "id": 12,
            "region_name": "Alessandra Deckow"
        },
        {
            "id": 13,
            "region_name": "Jarrod Gulgowski"
        },
        {
            "id": 14,
            "region_name": "Tito Labadie"
        },
        {
            "id": 15,
            "region_name": "Zora Zulauf"
        }
    ],
    "links": {
        "first": "http://localhost/regions?page=1",
        "last": "http://localhost/regions?page=2",
        "prev": null,
        "next": "http://localhost/regions?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 2,
        "path": "http://localhost/regions",
        "per_page": 15,
        "to": 15,
        "total": 20
    }
}
```

### Multiple counties
```JSON
{
    "data": [
        {
            "id": 1,
            "county_name": "Prof. Cullen Weber",
            "region_id": 1
        },
        {
            "id": 2,
            "county_name": "Jamie Hammes III",
            "region_id": 2
        },
        {
            "id": 3,
            "county_name": "Opal Smitham",
            "region_id": 3
        },
        {
            "id": 4,
            "county_name": "Jacinthe Torphy",
            "region_id": 4
        },
        {
            "id": 5,
            "county_name": "Reva Johns",
            "region_id": 5
        },
        {
            "id": 6,
            "county_name": "Mr. Darron Jacobi",
            "region_id": 6
        },
        {
            "id": 7,
            "county_name": "Bobbie Blick Sr.",
            "region_id": 7
        },
        {
            "id": 8,
            "county_name": "Laisha Lockman",
            "region_id": 8
        },
        {
            "id": 9,
            "county_name": "Madeline Welch",
            "region_id": 9
        },
        {
            "id": 10,
            "county_name": "Bernie Dibbert II",
            "region_id": 10
        },
        {
            "id": 11,
            "county_name": "Gertrude Rutherford",
            "region_id": 11
        },
        {
            "id": 12,
            "county_name": "Hillard Funk",
            "region_id": 12
        },
        {
            "id": 13,
            "county_name": "Greg Rohan",
            "region_id": 13
        },
        {
            "id": 14,
            "county_name": "Rossie Rohan",
            "region_id": 14
        },
        {
            "id": 15,
            "county_name": "Merritt D'Amore",
            "region_id": 15
        }
    ],
    "links": {
        "first": "http://localhost/counties?page=1",
        "last": "http://localhost/counties?page=2",
        "prev": null,
        "next": "http://localhost/counties?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 2,
        "path": "http://localhost/counties",
        "per_page": 15,
        "to": 15,
        "total": 20
    }
}
```

## Endpoints:
### Authentication:

```
POST /api/login
```

Example request body:

```
{
"email": "joe@doe.com",
"password": "password
}
```

No authentication requered, returns a token.

Required fields: `email`, `password`

### Logout

```
GET /api/logout
```

Authentication required. If successful, returns:
```JSON
{
    "success": {
        "message": "Successful Logout.",
        "data": []
    }
}
```

### RegionController
```
GET /regions
```

Authentication required, returns [multiple
regions](#multiple-regions)

### CountyController
```
GET /counties
```

Authentication required, returns [multiple
counties](#multiple-counties)
