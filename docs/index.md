# Welcome to Chanjo API docs

Chanjo Api aims to provide a RESTful backend system that can be
used by various front ends. It provides a link to the existing
chanjo database and uses [lumen framework] (https://lumen.laravel.com/).

For full documentation commands visit [mkdocs.org](https://mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
