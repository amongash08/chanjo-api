<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            'name' => 'admin',
            'description' => 'Administrator'
        ]);
        DB::table('groups')->insert([
            'name' => 'epi_admin',
            'description' => 'EPI Logistician - Admin'
        ]);
        DB::table('groups')->insert([
            'name' => 'epi',
            'description' => 'EPI Logistician'
        ]);
        //
    }
}
