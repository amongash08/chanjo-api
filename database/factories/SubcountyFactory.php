<?php

use Faker\Generator as Faker;

$factory->define(App\Subcounty::class, function (Faker $faker) {
    return [
        'subcounty_name' => $faker->name,
        'region_id' => function () {
            return factory(App\Region::class)->create()->id;
        },
        'county_id' => function () {
            return factory(App\County::class)->create()->id;
        },
        'total_population' => $faker->numberBetween($min = 1000, $max = 9000),
        'under_one_population' => $faker->numberBetween($min =1000, $max=5000),
        'women_population' => $faker->numberBetween($min = 1000, $max=5000)
    ];
});
