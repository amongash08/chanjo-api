<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Middleware\RoleMiddleware;
use App\Group;
use App\User;

class RoleMiddleWareTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        DB::statement("SET foreign_key_checks=0");
        DB::table('users_groups')->truncate();
        User::truncate();
        Group::truncate();
        DB::statement("SET foreign_key_checks=1");
        $this->seed('GroupsTableSeeder');

        \Route::middleware('role:'.Group::ADMIN)->any('/admin', function(){
            return 'OK';
        });
        \Route::middleware('role:'.Group::EPI_ADMIN)->any('/epi_admin', function(){
            return 'OK';
        });
        \Route::middleware('role:'.Group::EPI)->any('/epi', function(){
            return 'OK';
        });
    }

    public function test_admins_role_accepted()
    {
        $user = factory('App\User')->create();
        $admin_group = Group::where('name', 'admin')->firstOrFail();

        $user->roles()->attach($admin_group->id);

        $this->actingAs($user);

        $response = $this->get('/admin');
        $this->assertEquals($response->status(), 200);
        $this->assertEquals($response->getContent(), 'OK');
    }

    public function test_non_admins_role_not_accepted()
    {
        $user = factory('App\User')->create();

        $this->actingAs($user);

        $response = $this->get('/admin');
        $this->assertEquals($response->status(), 403);
        $this->assertNotEquals($response->getContent(), 'OK');
    }

    public function test_epi_admin_role_accepted()
    {
        $user = factory('App\User')->create();
        $group = Group::where('name', Group::EPI_ADMIN)->firstOrFail();

        $user->roles()->attach($group->id);

        $this->actingAs($user);

        $response = $this->get('/epi_admin');
        $this->assertEquals($response->status(), 200);
        $this->assertEquals($response->getContent(), 'OK');
    }

    public function test_non_epi_admin_role_not_accepted()
    {
        $user = factory('App\User')->create();

        $this->actingAs($user);

        $response = $this->get('/epi_admin');
        $this->assertEquals($response->status(), 403);
        $this->assertNotEquals($response->getContent(), 'OK');
    }
    public function test_epi_role_accepted()
    {
        $user = factory('App\User')->create();
        $group = Group::where('name', Group::EPI)->firstOrFail();

        $user->roles()->attach($group->id);

        $this->actingAs($user);

        $response = $this->get('/epi');
        $this->assertEquals($response->status(), 200);
        $this->assertEquals($response->getContent(), 'OK');
    }

    public function test_non_epi_role_not_accepted()
    {
        $user = factory('App\User')->create();

        $this->actingAs($user);

        $response = $this->get('/epi');
        $this->assertEquals($response->status(), 403);
        $this->assertNotEquals($response->getContent(), 'OK');
    }

}
