<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\User;
use App\Group;
use App\Facility;

class FacilityControllerTest extends TestCase{
    use DatabaseTransactions;

    protected function setUp(){
        parent::setUp();
        DB::statement("SET foreign_key_checks=0");
        DB::table('users_groups')->truncate();
        User::truncate();
        Group::truncate();
        Facility::truncate();
        DB::statement("SET foreign_key_checks=1");
        $this->seed('GroupsTableSeeder');
        for ($x = 0; $x < 20; $x++) {
            $facility = factory('App\Facility')->create();
        }
    }

    public function test_index_access_by_admin(){
        $user = factory('App\User')->create();
        $admin_group = Group::where('name', 'admin')->firstOrFail();
        $user->roles()->attach($admin_group->id);

        $token = JWTAuth::fromUser($user);
        $this->refreshApplication();

        $response = $this->actingAs($user)
                         ->get('/facilities', ['Authorization' => "Bearer $token"]);
        $response->assertStatus(200);
        $response_array = json_decode($response->getContent(), true);
        $this->assertEquals(15, count($response_array['data']));
        $meta_array = $response_array['meta'];
        $this->assertEquals(20, $meta_array['total']);
    }

}
