<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\Http\Controllers\RegionController;
use App\Region;
use App\County;
use App\User;
use App\Group;

class CountyControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        DB::statement("SET foreign_key_checks=0");
        DB::table('users_groups')->truncate();
        User::truncate();
        Group::truncate();
        County::truncate();
        Region::truncate();
        DB::statement("SET foreign_key_checks=1");
        $this->seed('GroupsTableSeeder');
        for ($x = 0; $x < 20; $x++) {
            $county = factory('App\County')->create();
        }
    }

    public function test_index_access_by_admin()
    {
        $user = factory('App\User')->create();
        $admin_group = Group::where('name', 'admin')->firstOrFail();
        $user->roles()->attach($admin_group->id);

        $token = JWTAuth::fromUser($user);
        $this->refreshApplication();

        $response = $this->actingAs($user)
                         ->get('/counties', ['Authorization' => "Bearer $token"]);
        $response->assertStatus(200);
        $response_array = json_decode($response->getContent(), true);
        $this->assertEquals(15, count($response_array['data']));
        $meta_array = $response_array['meta'];
        $this->assertEquals(20, $meta_array['total']);
    }

    public function test_index_cant_be_accessed_by_nonadmin()
    {
        $user = factory('App\User')->create();
        $token = JWTAuth::fromUser($user);
        $this->refreshApplication();

        $response = $this->actingAs($user)
                         ->get('/counties', ['Authorization' => "Bearer $token"]);
        $response->assertStatus(403);
    }
}
