<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\Http\Controllers\RegionController;
use App\Region;

class RegionControllerTest extends TestCase
{

    use DatabaseTransactions;

    protected function setUp(){
        parent::setUp();
        // this is added because transactions do not seem to work
        DB::statement("SET foreign_key_checks=0");
        Region::truncate();
        DB::statement("SET foreign_key_checks=1");

        for ($x = 0; $x < 20; $x++) {
            $region = factory('App\Region')->create();
        }
    }

    public function testIndex()
    {

        $user = factory('App\User')->create();
        $token = JWTAuth::fromUser($user);
        $this->refreshApplication();


        $response = $this->actingAs($user)
                         ->get('/regions', ['Authorization' => "Bearer $token"]);
        $response->assertStatus(200);
        $response_array = json_decode($response->getContent(), true);
        $this->assertEquals(15, count($response_array['data']));
        $meta_array = $response_array['meta'];
        $this->assertEquals(20, $meta_array['total']);
    }

    public function test_create_new_region(){
        $user = factory('App\User')->create();
        $token = JWTAuth::fromUser($user);
        $this->refreshApplication();

        $region = [
            'region_name' => 'Mariejois'
        ];
        $response = $this->actingAs($user)
            ->json('POST', 'regions/', $region, ['Authorization' => "Bearer $token"]);
        $response->assertStatus(200);
        $response_array = json_decode($response->getContent(), true);
        $this->assertEquals('Mariejois', $response_array['region_name']);
    }

    public function testBasicTest()
    {
        $this->assertTrue(true);
    }
}
