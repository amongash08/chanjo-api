<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'locations';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'location_name', 'nation_id','region_id','county_id',
      'subcounty_id','facility_id'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['json_location'];

  public function user_base()
  {

        return $this->hasMany('App\UserBase');

  }

  public function users()
  {
        return $this->hasManyThrough('App\User', 'App\UserBase');
  }


  public function transaction()
    {

        return $this->hasMany('App\Transaction');

    }

  public function region()
  {

        return $this->belongsTo('App\Region');

  }

  public function county()
  {

        return $this->belongsTo('App\County');

  }

  public function subcounty()
  {

        return $this->belongsTo('App\Subcounty');

  }

  public function facility()
  {

        return $this->belongsTo('App\Facility');

  }

  public function store()
  {

      return $this->hasOne('App\Store');

  }

  public function store_fridges()
  {

    return $this->hasManyThrough('App\StoreFridge', 'App\Store');

  }

  public function batch_balance()
  {

      return $this->hasMany('App\BatchBalance');

  }

}
