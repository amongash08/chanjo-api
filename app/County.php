<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'counties';

    /**
     * Model is not timestamped
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'county_name', 'region_id', 'total_population',
        'under_one_population','women_population'

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function sub_counties(){
        return $this->hasMany('App\Subcounty');
    }

    public function facilities(){
        return $this->hasMany('App\Facility');
    }

    public function region(){
        return $this->belongsTo('App\Region', 'region_id');
    }

    public function location()
    {

          return $this->hasOne('App\County');

    }


}
