<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'regions';

    /**
     * Model is not timestamped
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'region_name', 'total_population',
        'under_one_population','women_population'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function counties(){
        return $this->hasMany('App\County');
    }

    public function sub_counties(){
        return $this->hasMany('App\Subcounty');
    }

    public function facilities(){
        return $this->hasMany('App\Facility');
    }
    public function users(){
        return $this->hasMany('App\User');
    }
    
    public function location()
    {

          return $this->hasOne('App\Region');

    }

}
