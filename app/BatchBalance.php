<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchBalance extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'batch_balances';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'vaccine_id','batch','expiry_date','vvm','balance','transaction_id',
      'date'
  ];

  public function location()
  {

      return $this->belongsTo('App\Location');

  }

  public function vaccine()
  {

      return $this->belongsTo('App\Vaccine');

  }

  public function getBatchBalanceLocation()
  {
    $sub = DB::table('batch_balances')->select('vaccine_id')->max('transaction_id as transaction_id')->get();
    return $sub;

    // $balances = DB::table('batch_balances')
    //         ->join('contacts', 'users.id', '=', 'contacts.user_id')
    //         ->join('orders', 'users.id', '=', 'orders.user_id')
    //         ->select('users.*', 'contacts.phone', 'orders.price')
    //         ->get();


  }

}
