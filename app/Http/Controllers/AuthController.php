<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthController extends Controller
{

    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function index()
    {
        //

    }

    /**
       * Authenticate an user.
       *
       * @param Request $request
       * @return \Illuminate\Http\JsonResponse
       */
      public function login(Request $request)
      {
          $credentials = $request->only('email', 'password');

          $validator = Validator::make($credentials, [
              'email' => 'required|email',
              'password' => 'required'
          ]);

          if ($validator->fails()) {
              return response()
                  ->json([
                      'code' => 1,
                      'message' => 'Validation failed.',
                      'errors' => $validator->errors()
                  ], 422);
          }

          $token = JWTAuth::attempt($credentials);

          if ($token) {
              return response()->json(['token' => $token]);
          } else {
              return response()->json(['code' => 2, 'message' => 'Invalid credentials.'], 401);
          }
      }

      /**
       * Get the user by token.
       *
       * @param  Request  $request
       * @return \Illuminate\Http\JsonResponse
       */
      public function getUser(Request $request)
      {
          JWTAuth::setToken($request->input('token'));
          $user = JWTAuth::toUser();
          return response()->json($user);
      }

    public function logout()
    {
        //execute logout
        //dd(JWTAuth::parseToken()->getToken());
        JWTAuth::invalidate(JWTAuth::getToken());

        // Sentinel::logout(null, true);
        return Response::json([
            'success' => [
                'message' => 'Successful Logout.',
                'data' => [

                ]
            ]
        ], 200);


    }
}
