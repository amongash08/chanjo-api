<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Store;
use Response;
use App\BatchBalance;
use Illuminate\Support\Facades\DB;
// use App\Http\Resources\Stock as StockResource;

class StockController extends BaseController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

			// return Response::json($stores);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
			//
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
			//
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{

			 // return Response::json($store);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
			//
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
			//
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
			//
	}

	/**
	 * Get Stock by specific criteria.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getStockDoses(Request $request)
	{

		$level = $request->input('level');

		switch ($level) {

			case 'national':

			$sub = DB::table('batch_balances')->groupBy('location_id','vaccine_id','batch')
			->get(['vaccine_id', DB::raw('MAX(transaction_id) as transaction_id')]);

			$main = DB::table( DB::raw("({$sub->toSql()}) as sub") )->mergeBindings($sub->getQuery())->where('vaccine_id',1)->count();

			return $main;

				break;

			case 'region':

				return 'region';

				break;

			case 'county':

				return 'county';

				break;

			case 'subcounty':

				return 'subcounty';

				break;

			default:

				return 'national';

				break;


	}

	}
}
