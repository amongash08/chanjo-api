<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserBase;
use App\Level;
use Mail;
use App\Mail\WeeklyReport;
use App\Http\Resources\User as UserResource;
use JonnyW\PhantomJs\Client;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Jobs\WeeklyEmailJob;
use Log;
use App\Location;
use Response;
use App\Http\Resources\MosLocation as MosLocationResource;

class ReportController extends Controller
{

    function __construct(){

    }

    public function stocksWeeklyReport()
    {
      $levels = Level::all();

      foreach ($levels as $key => $value) {
        $level = $value->id;

        $users = User::OfUserByLevel($level)->active()->get();
        $usersArray =collect(UserResource::collection($users));

        switch ($value->name) {

          case 'National':
            $link = 'https://www.epikenya.org/reports/nationalAndRegionalWeeklyReport/';
            $bcc ='';
            $this->sendEmail($usersArray,$link,$bcc);
            break;

          case 'Region':
            $link = 'https://www.epikenya.org/reports/nationalAndRegionalWeeklyReport/';
            $bcc ='';
            $this->sendEmail($usersArray,$link,$bcc);
            break;

          case 'County':
            $link = 'https://www.epikenya.org/reports/countyAndRegionalWeeklyReport/';
            $bcc ='';
            $this->sendEmail($usersArray,$link,$bcc);

            break;

          case 'Sub County':
            $link = 'https://www.epikenya.org/reports/countyAndRegionalWeeklyReport/';
            $bcc ='';
            $this->sendEmail($usersArray,$link,$bcc);
            break;

          default:
            // code...
            break;
        }
      }

    }

    public function sendEmail($usersArray,$link)
    {

      $format = 'Y-M-d';
      $today = Carbon::today()->format($format);

      // $filesArray = [];
      // $recipientArray = [];

      foreach ($usersArray as $user) {

        //generate reports
        $report_url = $link.$user['location']->location->id;
        $recipient = $user['email'];
        $subject = $user['location']->location->location_name .'_Weekly Stock Status_'.$today;
        $report_name = str_replace(' ', '_', $subject);
        $recipient_name = $user['name'];
        $recipient_location = $user['location']->location->location_name ;
        $this-> generateReport($report_url,$report_name);


        $attachment = storage_path('app/public/'.$report_name.'.pdf');
        //send mail

        $delay = now()->addSeconds(50);

        dispatch(new WeeklyEmailJob($recipient,$subject,$recipient_name,$attachment,$recipient_location))->delay($delay);
        Log::useDailyFiles(storage_path().'/logs/activity.log');
        Log::info('Email Queued for - '.$recipient);

        // Mail::to($recipient)->later($delay, new WeeklyReport($subject,$recipient_name,$attachment,$recipient_location));




      }


}

    public function generateReport($report_url, $report_name){

      $client = Client::getInstance();
      $client->getEngine()->setPath('/usr/local/bin/phantomjs');
      $client->isLazy();

      $file = storage_path('app/public/'.$report_name.'.pdf');
      $delay = 10; //  seconds

      $request = $client->getMessageFactory()->createPdfRequest($report_url);
      $request->setDelay($delay);
      $request->setOutputFile($file);
      $request->setFormat('A3');
      $request->setOrientation('landscape');
      $request->setMargin('1cm');

      $response = $client->getMessageFactory()->createResponse();

      $request->setOutputFile($file);

      $client->send($request, $response);
      Log::useDailyFiles(storage_path().'/logs/activity.log');
      Log::notice('Generated report - '.$report_name);


    }

    public function cleanStorage(){

      $directory = 'public';
      $files = Storage::allFiles($directory);
      $directories = Storage::directories($directory);

      Log::useDailyFiles(storage_path().'/logs/activity.log');

      if (!empty($files)) {

        Storage::delete($files);
        Log::info('Files were Deleted');

      }else{

        Log::info('No Files to Delete');

      }

    }

    public function mos(){

      $locations = Location::paginate();
      return MosLocationResource::collection($locations);

    }
}
