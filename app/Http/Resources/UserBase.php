<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Location as LocationResource;

class UserBase extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return
      [

      'id' => $this->id,
      'user_id' => $this->user_id,
      'level_id' => $this->level_id,
      'location' => new LocationResource($this->location),

      ];
    }
}
