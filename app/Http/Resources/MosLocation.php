<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Faker\Factory as Faker;

class MosLocation extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $faker = Faker::create();
        return [
          'id' => $this->id,
          'location_name' => $this->location_name,
          'vaccine_mos' => ['BCG'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'ROTA'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'TT'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'OPV'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'IPV'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'PCV'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'DPT'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'MR'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 6),
                            'YF'=>$faker->randomFloat($nbMaxDecimals = 1, $min = 0, $max = 2)],
          'nation_id' => $this->nation_id,
          'region' => $this->region,
          'county' => $this->county,
          'subcounty' => $this->subcounty,
          'facility' => $this->facility

      ];

    }
}
