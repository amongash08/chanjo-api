<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Fridge extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'model' => $this->Model,
          'manufacturer' => $this->Manufacturer,
          'technology_type' => $this-> {'Technology Type'},
          'vaccine_storage_volume' => ['volume' => (float)$this->{'Vaccine storage volume (L)'} , 'units' => 'Lts'],
          'holdover_period' => ['period' => (float)$this->{'Holdover period (hrs)'} ,'units' => 'Hrs'],
          'freezer' => $this->Freezer_capacity,

      ];

    }
}
