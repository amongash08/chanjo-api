<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Location as LocationResource;


class Facility extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'organisationunitid' => $this->organisationunitid,
          'mfl' => $this->mfl,
          'facility_name' => $this->facility_name,
          'immunizing_status' => $this->immunizing_status,
          'operation_status' => $this->operation_status,
          'owner' => $this->owner,
          // 'store_infor' => new LocationResource($this->location)

      ];

    }
}
