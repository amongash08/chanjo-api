<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Location extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'location_name' => $this->location_name,
          'nation_id' => $this->nation_id,
          'region' => $this->region,
          'county' => $this->county,
          'subcounty' => $this->subcounty,
          'facility' => $this->facility

      ];

    }
}
