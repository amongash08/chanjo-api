<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\UserBase as UserBaseResource;

class User extends Resource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return
    [

    'id' => $this->id,
    'username' => $this->username,
    'email' => $this->email,
    'phone' => $this->phone,
    'name' => ucwords(strtolower($this->first_name)).' '.ucwords(strtolower($this->last_name)),
    'location' => new UserBaseResource($this->user_base),

    ];

  }
}
