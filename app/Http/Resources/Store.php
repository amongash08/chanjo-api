<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Store extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'store_name' => $this->name,
          'location_id' => $this->location,
          'catchment_population' => $this->catchment_population,
          'live_birth_population' => $this->live_birth_population,
          'pop_pregnant_women' => $this->pop_pregnant_women,
          'pop_surviving_infants' => $this->pop_surviving_infants,
          'pop_adolescent_girls' => $this->pop_adolescent_girls,
          // 'fridges' => $this->store_fridges,
          'cold_boxes' => $this->cold_boxes,
          'vaccine_carriers' => $this->vaccine_carriers,
          'ice_packs' => $this->ice_packs,
          'time_to_store' => $this->time_to_store,
          'nearest_town' => $this->nearest_town,
          'electricity_status' => $this->electricity_status,
          'roof_type' => $this->roof_type,
          'manager_name' => $this->manager_name,
          'distance_to_store' => $this->distance_to_store,
          'manager_phone' => $this->manager_phone,
          'road_access' => $this->road_access,
          'road_quality' => $this->road_quality,
          'special_access' => $this->special_access,
          'active' => $this->active
      ];
    }


}
