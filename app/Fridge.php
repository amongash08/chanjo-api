<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fridge extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'fridges';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'model','manufacturer','Technology Type','Vaccine storage volume (L)','Holdover period (hrs)','Freezer_capacity',
      'Purchase price/ unit (USD)'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['Freight charges - to port (USD)','Local levies and in-country transport (USD)','Installation charges (USD)',
  'Training costs (USD)','Total installed cost/ Capex (USD)','Annual energy costs (USD)','Annual maintenance costs (USD)',
  'Total operational cost (USD)','TCO (USD)','TCO/ Ltr (USD)'];

  public function storefridge()
  {

      return $this->belongsTo('App\StoreFridge');

  }
}
