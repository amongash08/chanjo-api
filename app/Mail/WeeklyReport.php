<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WeeklyReport extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    // public $message;
    // public $recipient;
    public $attachment;
    // public $recipient_location;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$attachment)
    {
        $this->subject = $subject;
        // $this->message = $message;
        // $this->recipient_name = $recipient_name;
        $this->attachment = $attachment;
        // $this->recipient_location = $recipient_location;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('reports@epikenya.org','Chanjo')
                    ->subject($this->subject)
                    ->view('emails.reports.stocksWeekly')
                    ->attach($this->attachment, ['mime' => 'application/pdf',]);
    }
}
