<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBase extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'user_base';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
      'user_id', 'level_id', 'location_id',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];

    public function user()
    {

          return $this->belongsTo('App\User');

    }

    public function location()
    {

          return $this->belongsTo('App\Location');

    }

    public function scopeOfLevelId($query, $level)
    {

        return $query->where('level_id', $level);

    }



}
