<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    const ADMIN = 'admin';
    const EPI_ADMIN = 'epi_admin';
    const EPI = 'epi';
    
    public $timestamps = False;

    protected $table = 'groups';

    protected $fillable = [
        'name', 'description'
    ];
}
