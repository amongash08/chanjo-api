<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facilities';
    /**
     * Model is not timestamped
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'organisationunitid','mfl','facility_name','subcounty_id','county_id','region_id',
        'total_population','under_one_population','women_population','catchment_population',
        'live_birth_population','nearest_town','nearest_town_distance','nearest_store_distance',
        'cold_boxes','vaccine_carriers','ice_packs','immunizing_status','operation_status','owner'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function county(){
        return $this->belongsTo('App\County');
    }

    public function subcounty(){
        return $this->belongsTo('App\Subcounty');
    }

    public function region(){
        return $this->belongsTo('App\Region');
    }

    public function location()
    {
          return $this->hasOne('App\Location');
    }

}
