<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\WeeklyReport;

class WeeklyEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $subject;
    // public $message;
    public $recipient;
    public $recipient_name;
    protected $attachment;
    public $recipient_location;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($recipient,$subject,$recipient_name,$attachment,$recipient_location)
     {
         $this->subject = $subject;
         $this->recipient = $recipient;
         $this->recipient_name = $recipient_name;
         $this->attachment = $attachment;
         $this->recipient_location = $recipient_location;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->recipient)->send(new WeeklyReport($this->subject,$this->attachment));
    }
}
