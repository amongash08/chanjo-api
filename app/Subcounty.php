<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcounty extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subcounties';

    /**
     * Model is not timestamped
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'subcounty_name', 'region_id','county_id','total_population',
        'under_one_population','women_population'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function county(){
        return $this->belongsTo('App\County');
    }

    public function region(){
        return $this->belongsTo('App\Region');
    }

    public function scopeCounty($query,$county){
        $county_id = County::whereName($county)->first()->id;
        return $query->where('county_id',$county_id);
    }

    public function facilities(){
        return $this->hasMany('App\Facility' ,'subcounty_id');
    }

    public function location()
    {

        return $this->hasOne('App\Subcounty');

    }



}
