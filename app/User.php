<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','first_name','last_name','phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
    * Get the identifier that will be stored in the subject claim of the JWT.
    *
    * @return mixed
    */
   public function getJWTIdentifier()
   {
       return $this->getKey();
   }
   /**
    * Return a key value array, containing any custom claims to be added to the JWT.
    *
    * @return array
    */
   public function getJWTCustomClaims()
   {
       return [];
   }

   public function user_base()
   {
       return $this->hasOne('App\UserBase');
   }

   public function transactions()
    {
          return $this->hasMany('App\Transaction');
    }


    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Scope a query to only include users by level.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Use level $level
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfUserByLevel($query,$level)
    {
        return $query->whereHas('user_base', function ($q) use($level){
            $q->OfLevelId($level);
        });
    }

    public function roles()
    {
        return $this->belongsToMany('App\Group', 'users_groups', 'user_id', 'group_id');
    }

    public function is($roleName)
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == $roleName){
                return true;
            }
        }
        return false;
    }

}
